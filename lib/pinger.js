const interval = require('interval-promise')
const puppeteer = require('puppeteer');

const TEST_SITES = require('../config').TEST_SITES
const SendSMS = require('./InfoBip')


module.exports = {
  startPing: async () => {
    const browser = await puppeteer.launch({
      executablePath: '/usr/bin/chromium-browser',
      args: ['--no-sandbox', '--disable-setuid-sandbox']
    })
    await testSites(browser)
    interval(async () => {
      await testSites(browser)
    }, 60 * 1000)
  }
}

async function testSites(browser) {
  const page = await browser.newPage()
  for (let i = 0; i < TEST_SITES.length; i++) {
    const site = TEST_SITES[i]
    try {
      await page.goto(site.url, {
        waitUntil: [
          'load',
          'networkidle0'
        ]
      })

      const html = await page.evaluate(() => {
        return document.querySelector('html').innerHTML
      })
      if (site.searchString && html.indexOf(site.searchString) === -1) {
        SendSMS({
          site: site.url,
          message: smartTrim(`Is responding, but "${site.searchString}" not found!`)
        })
      }
    } catch (error) {
      SendSMS({
        message: smartTrim(`${site.url} - ${error.message}`)
      })
    }
    await page.close()
  }
}

function smartTrim(str, length = 150, appendix = '...', delim = ' ') {
  if (str.length <= length) return str

  var trimmedStr = str.substr(0, length + delim.length)

  var lastDelimIndex = trimmedStr.lastIndexOf(delim)
  if (lastDelimIndex >= 0) trimmedStr = trimmedStr.substr(0, lastDelimIndex)

  if (trimmedStr) trimmedStr += appendix
  return trimmedStr
}
