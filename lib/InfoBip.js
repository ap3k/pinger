const axios = require('axios')
const config = require('../config').INFOBIP

module.exports = ({ site, message }) => {
  if (site) {
    message = `${site} ${message}`
  }
  console.error(message)
  axios.post('https://epkg2.api.infobip.com/sms/1/text/single', {
    to: config.phone,
    text: message
  }, {
    headers: {
      'Authorization': `Basic ${config.auth}`,
    }
  })
}
