module.exports = {
  INFOBIP: {
    phone: process.env.PHONE_NR,
    auth: process.env.INFOBIP_AUTH_KEY,
  },
  TEST_SITES: [
    {
      url: 'https://ap3k.pro',
      searchString: 'code artisans'
    }
  ],
}
