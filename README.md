# Pinger
## Description
Project used to check if pages are up and running. 

Supports checking for certain content string on pages. 

Also works with SPAs (Single Page Applications) Vue, React, Angular etc

## How to use
1) `git clone https://gitlab.com/ap3k/pinger.git`
2) `cd pinger`
3) `npm ci` or `npm i`
4) Edit `./config.js` with favorite text editor (config example below)
5) Start with node `USERNAME=test PASSWORD=test PHONE_NR=+123456789 INFOBIP_AUTH_KEY=some_KEY node index.js` <br>
or with pm2 `USERNAME=test PASSWORD=test PHONE_NR=+123456789 INFOBIP_AUTH_KEY=some_KEY pm2 index.js`
6) In case you have no access to server and you need to stop pinger sending SMSes there is built in endpoint to forceby kill Pinger `/forcekill` <br>
`/forcekill` is configurable to be behind basic auth with `USERNAME` and `PASSWORD` env variables<br>
Go to step 5 to start Pinger again

```js
// config.js example
module.exports = {
  INFOBIP: {
    phone: process.env.PHONE_NR,
    auth: process.env.INFOBIP_AUTH_KEY,
  },
  TEST_SITES: [
    {
      url: 'http://my.site.test/',
      searchString: 'Example string'
    },
    {
      url: 'https://google.com'
      // If no searchString or searchString: '' then only status code is checked
    }
  ],
}
```

## Configurable env variables

> PORT - Port to listen on, defaults to 3001

> USERNAME - Forcekill endpoint basic auth username, defaults to '' (empty string)

> PASSWORD - Forcekill endpoint basic auth password, defaults to '' (empty string)

> PHONE_NR - Phone nr where to send SMS

> INFOBIP_AUTH_KEY - API key for Infobip

----------

## TODO
* Create Docker image
