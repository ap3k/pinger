const pinger = require('./lib/pinger')
const TEST_SITES = require('./config').TEST_SITES
const createServer = require('http').createServer

const PORT = process.env.PORT || 3001

pinger.startPing()

createServer((req, res) => {
  res.writeHead(200, { 'content-type': 'application/json' })
  if (req.url === '/forcekill') {
    var userpass = new Buffer.from((req.headers.authorization || '').split(' ')[1] || '', 'base64').toString();
    if (userpass !== `${process.env.USERNAME || ''}:${process.env.PASSWORD || ''}`) {
      res.writeHead(401, { 'WWW-Authenticate': 'Basic realm="nope"' });
      res.end('Unauthorized: Access is denied');
      return;
    }
    res.write(JSON.stringify('Forceby killed'))
    res.end(() => {
      process.exit()
    })
    return
  }
  res.write(JSON.stringify(TEST_SITES))
  res.end()
}).listen(PORT)

process.on('exit', function () {
  console.error('Force killed')
})
